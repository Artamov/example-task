<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>

    <div class="container-fluid">
        <h3>Все файлы</h3>
        @foreach($files as $file)
            <h5 class="mt-3">Информация о файле</h5>
            <p><b>Название</b>: {{ $file->name }}</p>
            <p><b>Описание</b>: {{ $file->description }}</p>
            <p><b>Файл:</b>: <a href="{{ \Illuminate\Support\Facades\Storage::url($file->src) }}">Открыть</a></p>
            <hr>
        @endforeach
        <h3 class="mt-5">Загрузка файла</h3>
        <form action="{{ route('store-file') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="mb-3">
                <label for="formFile" class="form-label">Выберите файл</label>
                <input class="form-control" type="file" id="formFile" name="file">
                @error('file')
                    <div class="error">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">Название</label>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="file_name">
                @error('file_name')
                    <div class="error">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Описание</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="file_description"></textarea>
                @error('file_description')
                <div class="error">{{ $message }}</div>
                @enderror
            </div>
            <button class="btn btn-success">Сохранить</button>
        </form>
    </div>
</body>
</html>
