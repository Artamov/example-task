<?php

namespace App\Http\Controllers;

use App\Http\Requests\FileStoreRequest;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    public function index()
    {
        return view('upload_file', [
            'files' => File::all()
        ]);
    }

    public function store(FileStoreRequest $request)
    {
        if($request->has('file')) {
            $fileHash = sha1_file( request()->file('file')->path());
            if($file = File::where('hash', $fileHash)->first()) {
                return redirect()->route('show', $file->id);
            }

            $fileName = time() . Str::random(5) . '.' . request()->file('file')->getClientOriginalExtension();
            $path = $request->file('file')->storeAs(
                'public/files', $fileName
            );

            $description = $request->input('file_description');
            File::create([
                'name' => $fileName,
                'description' => $description,
                'hash' => $fileHash,
                'src' => $path
            ]);
            return \redirect()->route('index');
        }
    }

    public function show(File $file)
    {

        return view('show_file', [
            'file' => $file,
            'path' => Storage::url($file->src)
        ]);
    }
}
